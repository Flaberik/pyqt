import os
import json
from PIL import Image, ImageDraw


class parserFromImg:
    def __init__(self):
        self.path = os.getcwd() + '/images/'
        self.charArr = []


        self.parseCharArr()
        self.data = {"charList": self.charArr,"data": []}
        # self.data["data"].append({"charCount": len(self.charArr)})
        for char in self.charArr:
            self.parseImages(char)
        self.writeToFile()

        # print(len(self.data['data'][0]['arr']))



    def writeToJson(self):
        pass

    def writeToFile(self):
        with open('trainingData.json', 'w') as f:
            f.write(json.dumps(self.data))

    def parseCharArr(self):
        self.charArr = os.listdir(self.path)

    def parseImages(self, folder):
        full_path = self.path + folder + '/'
        imgList = []
        for imgName in os.listdir(full_path):
            if('.directory' not in imgName):
                imgList.append(imgName)

        self.data["data"].append({"char": folder, "arr": [], "imgCount": len(imgList)})

        for imgName in imgList:
            img = Image.open('images/'+folder+'/'+imgName)
            imgArr = []
            pixArr = img.load()
            for i in range(img.size[0]):
                row = []
                for j in range(img.size[1]):
                    if(pixArr[j, i][0] == 0 and pixArr[j, i][1] == 0 and pixArr[j, i][2] == 0):
                        row.append(1)
                    else:
                        row.append(0)

                imgArr.append(row)

            self.data['data'][self.charArr.index(folder)]['arr'].append(imgArr)



def main():
    parser = parserFromImg()
    # print(parser.charArr)


if __name__ == '__main__':
    main()
