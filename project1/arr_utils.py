import sys


class utils:
    def __init__(self):
        pass

    def getMin(self, array, exclude=None):
        min = sys.maxsize

        for arr in array:
            for element in arr:
                if (element != exclude and element < min):
                    min = element
        return min

    def getMean(self, array, exclude=None):
        sum = 0.0
        count = 0
        for arr in array:
            for element in arr:
                if (element != exclude):
                    sum += element
                    count += 1
        return sum / count
