import json
import math
import os
import random
import sys

from PIL import Image, ImageDraw
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QApplication, QMainWindow

from arr_utils import utils
from forms.Ui_untitled import *


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None, *args, **kwargs):
        QMainWindow.__init__(self)

        self.setupUi(self)
        self.initUi()

        # print(sys.maxsize-1)

        self.bigdata = {"data": []}

        self.minX = 13.0
        self.minY = 60.0

        self.wid = 324.0
        self.hei = 324.0

        self.cd  = 128
        self.minCells = 16

        self.endPoint = QPoint(0, 0)
        self.beginPoint = QPoint(self.wid, self.hei)

        self.gridData = []
        self.minGridData = []
        self.charArr = []

        self.beginX = self.bigCells
        self.beginY = self.bigCells

        self.endX = 0
        self.endY = 0

        self.drawing = False

        self.drawing2 = False
        self.lastPos = QPoint()

        self.minImage = QImage(QSize(int(self.wid / 2), int(self.hei / 2)), QImage.Format_RGB32)
        self.minImage.fill(Qt.white)

        self.pillImage = None

        self.bigImage = QImage(QSize(self.wid, self.hei), QImage.Format_RGB32)
        self.bigImage.fill(Qt.white)

        self.np_arrs = []

        self.trainingData = {}

        for i in range(self.minCells):
            self.minGridData.append([0 for j in range(self.minCells)])

        for i in range(self.bigCells):
            self.gridData.append([0 for j in range(self.bigCells)])


    def initUi(self):
        self.btnAddChar.clicked.connect(self.addCharToData)
        self.btnClear.clicked.connect(self.clearGrid)
        self.btnGo.clicked.connect(self.scaling)
        self.btnTrain.clicked.connect(self.train)
        self.autoTrain.clicked.connect(self.trainFromTrainingData)


    def trainFromTrainingData(self):
        self.readTrainingData()
        self.addCharToData("NaN")

        charList = self.trainingData['charList']
        for char in charList:
            self.addCharToData(char)

        for i in range(len(self.trainingData['data'])):
            char = self.trainingData['data'][i]['char']
            for j in range(len(self.trainingData['data'][i]['arr'])):
                self.minGridData = self.trainingData['data'][i]['arr'][j]
                print("")
                for ii in range(self.minCells):
                    row = ''
                    for jj in range(self.minCells):
                        if(self.minGridData[ii][jj] == 1):
                            row+='#'
                        else:
                            row+=' '
                    print(row)

                self.train(char)
                print('char: {} in: {} / {} | img: {} / {}'.format(char,i+1,len(self.charArr)-1,j+1,len(self.trainingData['data'][i]['arr'])))
                # self.update()
            # self.lineEdit.setText(char)
            # print(char)

    def train(self, char=False):
        if(char is False):
            char = self.lineEdit.text()

        if (len(char) > 0):
            charId = -1
            try:
                charId = self.charArr.index(char)
            except:
                pass

            if (charId > -1):

                step = 1000.0
                sigmaFindChar = self.sig(self.calcuate_sum(charId))

                error = float(1.0e-22 - sigmaFindChar) / step

                res = self.search()
                charOutput = res[0]
                maxOutput = res[1]

                # print(charOutput)

                if (charOutput == self.charArr[charId]):
                    # print(maxOutput)
                    pass
                else:
                    # print(str(maxOutput) + ' ' + str(sigmaFindChar))

                    error = float(maxOutput - sigmaFindChar)
                    if ('e' in str(error)):
                        countZeroToError = int(str(error)[str(error).index('e') + 2:len(str(error))])
                        error = float(error * self.f1(countZeroToError - 5))
                        # print(float(error * self.f1(countZeroToError/2)))]

                stopTrain = False
                while (not stopTrain):
                    for i in range(self.minCells):
                        for j in range(self.minCells):
                            if (self.minGridData[i][j] == 1):
                                self.bigdata['data'][charId]['arr'][i][j] += error
                            elif (self.minGridData[i][j] == 0):
                                self.bigdata['data'][charId]['arr'][i][j] -= error/2
                            else:
                                pass
                                # print(self.minGridData[i][j])

                    print('(Training loop) char: {} | sigma: {}'.format(self.lineEdit.text(), self.sig(self.calcuate_sum(charId))))
                    if (self.search()[0] == char):
                        # print('find: ' + self.lineEdit.text())
                        # print('charList: {}'.format(self.charArr))
                        stopTrain = True

                self.bigdata['data'][charId]['era'] += 1

                # print('error: {}'.format(error))
                # print('sigmaOutput: {}'.format(sigmaFindChar))

                # self.writeBigdataToFile()
                # print()

    def search(self):
        maxOutput = -10.0
        charOutput = ''
        for i in range(len(self.charArr)):
            sum = self.calcuate_sum(i)
            output = self.sig(sum)

            if (maxOutput < output):
                maxOutput = output
                charOutput = self.bigdata['data'][i]['char']
        return (charOutput, maxOutput)

    def f1(self, number):
        output = 1
        for _ in range(int(number)):
            output *= 10
        return output

    def calcuate_sum(self, charId):
        sum_weights = 0.0
        arr = self.bigdata['data'][charId]['arr']
        for i in range(self.minCells):
            for j in range(self.minCells):
                if (self.minGridData[i][j] != 0):
                    sum_weights = sum_weights + 1.0 * arr[i][j]
        return sum_weights

    def testingPil(self, size):
        img = Image.new("RGBA", (size.width() + 2, size.height() + 2), (255, 255, 255))
        draw = ImageDraw.Draw(img)

        beginX = int(self.beginPoint.x() - 1 - self.minX)
        endX = int(self.endPoint.x() + 1 - self.minX)
        beginY = int(self.beginPoint.y() - 1 - self.minY)
        endY = int(self.endPoint.y() + 1 - self.minY)

        for i in range(beginX, endX):
            for j in range(beginY, endY):
                try:
                    draw.point((i - beginX, j - beginY), self.bigImage.pixelColor(i, j).getRgb())
                except:
                    pass

        img = img.resize((self.minCells, self.minCells), Image.CUBIC)
        imgOutput = Image.new("RGB", (self.minCells, self.minCells), (255, 255, 255))
        draw = ImageDraw.Draw(imgOutput)

        pix = img.load()

        for i in range(self.minCells):
            for j in range(self.minCells):
                if (pix[j, i][0] < 240 and pix[j, i][1] < 240 and pix[j, i][2] < 240):
                    draw.point((j, i), (0, 0, 0, 255))
                    self.minGridData[i][j] = 1

        if (len(self.lineEdit.text()) > 0):
            path = os.getcwd() + '/images/'
            try:
                os.mkdir(path + self.lineEdit.text())
            except:
                pass
            name = str(random.random()).replace('.', '')
            imgOutput.save(path + '/' + self.lineEdit.text() + '/' + name + '.png')

        self.pillImage = imgOutput = imgOutput.resize((int(self.wid / 2), int(self.hei / 2)), Image.CUBIC)
        pix = imgOutput.load()

        for i in range(imgOutput.size[0]):
            for j in range(imgOutput.size[1]):
                self.minImage.setPixelColor(j, i, QColor(pix[j, i][0], pix[j, i][1], pix[j, i][2]))

        self.update()

    def scaling(self):

        if (True):
            self.minGridData = []
            for i in range(self.minCells):
                self.minGridData.append([0 for j in range(self.minCells)])

            wid = self.endX - self.beginX
            hei = self.endY - self.beginY

            size = QSize(self.endPoint.x() - self.beginPoint.x(), self.endPoint.y() - self.beginPoint.y())

            self.testingPil(size)

            max_th = 0.0
            last_char = 'Null'

            for i in range(len(self.charArr)):
                sum = self.calcuate_sum(i)
                th = self.sig(sum)
                # print('later: {} | sigma: {}'.format(self.bigdata['data'][i]['char'],1 / (1/2 + math.exp(-1 * sum))))
                if (max_th < th):
                    max_th = th
                    last_char = self.bigdata['data'][i]['char']

            print('char: {} | sigma: {}'.format(last_char, max_th))

            for arr in self.minGridData:
                arr1 = [[], [], [], []]
                for block in range(4):
                    arr1[block] = arr[block * 4:(block + 1) * 4]

    def sig(self, sum):
        return (1 / (1 / 2 + math.exp(-1 * sum)))

    def th(self, x):
        return ((math.exp(x) - math.exp(-x)) / (math.exp(x) + math.exp(-x)))

    def addCharToData(self, char=None):
        if (type(char) == bool):
            char = self.lineEdit.text()
        if (len(char) > 0):
            try:
                self.charArr.index(char)
            except:
                print('newChar: ' + char)
                self.charArr.append(char)
                id = self.charArr.index(char) + 1
                arr = []
                for i in range(self.minCells):
                    arr.append([-1 - id / 1000 for i in range(self.minCells)])

                self.bigdata['data'].append({"char": str(char), "arr": arr, 'era': 0})
                self.lineEdit.setText('')
                self.writeBigdataToFile()

        # print(self.charArr)

    def clearGrid(self):
        self.gridData = []
        self.minGridData = []
        self.drawing = False

        self.beginX = self.bigCells
        self.beginY = self.bigCells
        self.endX = 0
        self.endY = 0

        self.endPoint = QPoint(0, 0)
        self.beginPoint = QPoint(self.wid, self.hei)

        self.bigImage.fill(Qt.white)

        for i in range(self.minCells):
            self.minGridData.append([0 for j in range(self.minCells)])

        for i in range(self.bigCells):
            self.gridData.append([0 for j in range(self.bigCells)])
        self.repaint()

    def mousePressEvent(self, event: QtGui.QMouseEvent):
        if (event.button() == Qt.LeftButton):
            self.lastPos = (event.pos() - QPoint(self.minX, self.minY))
            self.drawing2 = True

    def mouseMoveEvent(self, event: QtGui.QMouseEvent):
        if ((event.buttons() & Qt.LeftButton) & self.drawing2):
            painter = QPainter(self.bigImage)
            painter.setPen(QPen(Qt.black, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
            painter.drawLine(self.lastPos, (event.pos() - QPoint(self.minX, self.minY)))
            self.lastPos = (event.pos() - QPoint(self.minX, self.minY))

            if (self.endPoint.x() < event.pos().x()):
                self.endPoint = QPoint(event.pos().x(), self.endPoint.y())
            if (self.endPoint.y() < event.pos().y()):
                self.endPoint = QPoint(self.endPoint.x(), event.pos().y())

            if (self.beginPoint.x() > event.pos().x()):
                self.beginPoint = QPoint(event.pos().x(), self.beginPoint.y())
            if (self.beginPoint.y() > event.pos().y()):
                self.beginPoint = QPoint(self.beginPoint.x(), event.pos().y())

            self.repaint()

    def mouseReleaseEvent(self, event: QtGui.QMouseEvent):
        if (event.button() == Qt.LeftButton):
            self.drawing2 = False

    def paintEvent(self, e):
        canvas = QPainter(self)
        canvas.drawImage(QRect(self.minX, self.minY, self.wid, self.hei), self.bigImage, self.bigImage.rect())

        minImg = QPainter(self)
        minImg.drawImage(QRect(self.minX + self.wid + 10, self.minY + self.hei / 2, self.wid / 2, self.hei / 2),
                         self.minImage, self.minImage.rect())

        if (self.drawing2):
            qp = QPainter()
            qp.begin(self)
            qp.setBrush(QColor(255, 0, 0, 0))
            qp.setPen(QPen(Qt.red, 1, Qt.SolidLine))
            qp.drawRect(self.beginPoint.x(), self.beginPoint.y(), self.endPoint.x() - self.beginPoint.x(),
                        self.endPoint.y() - self.beginPoint.y())
            qp.end()

    def readTrainingData(self):
        jsonStr = ''
        with open('trainingData.json', 'r') as f:
            jsonStr = f.read()

        jsonStr.replace('\n', '')
        jsonStr.replace(' ', '')

        self.trainingData = json.loads(jsonStr)
        # self.charArr = self.trainingData['charList']

    def readBigdataToFile(self):
        jsonStr = ''
        with open('bigdata.json', 'r') as f:
            jsonStr = f.read()

        jsonStr.replace('\n', '')
        jsonStr.replace(' ', '')

        self.bigdata = json.loads(jsonStr)

        for obj in self.bigdata['data']:
            # self.np_arrs.append(np.full((self.minCells, self.minCells), -0.05))
            self.charArr.append(obj['char'])

            # for i in range(0, len(obj['arr'])):
            #     for j in range(0, len(obj['arr'][i])):
            #         self.np_arrs[len(self.np_arrs) - 1][i, j] = obj['arr'][i][j]

        ut = utils()
        # for arr in self.np_arrs:
        #     print('среднее: {} | минимальное: {} | максимальное: {}'.format(ut.getMean(array=arr, exclude=-0.05),
        #                                                                     ut.getMin(array=arr, exclude=-0.05),
        #                                                                     arr.max()))

    def writeBigdataToFile(self):
        with open('bigdata.json', 'w') as f:
            f.write(json.dumps(self.bigdata))


def main():
    app = QApplication(sys.argv)
    main = MainWindow()
    main.setWindowTitle('TiTLe')
    main.move(250, 250)
    main.show()
    main.readBigdataToFile()
    ex = app.exec_()
    if(ex == 0):
        main.writeBigdataToFile()
    sys.exit(ex)


if __name__ == '__main__':
    main()
