# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/oem/projects/pyqt/project1/forms/untitled.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowModality(QtCore.Qt.NonModal)
        MainWindow.setEnabled(True)
        MainWindow.resize(597, 491)
        MainWindow.setStyleSheet("")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.number = QtWidgets.QLCDNumber(self.centralwidget)
        self.number.setGeometry(QtCore.QRect(140, 0, 51, 23))
        self.number.setObjectName("number")
        self.btnClear = QtWidgets.QPushButton(self.centralwidget)
        self.btnClear.setGeometry(QtCore.QRect(50, 30, 40, 23))
        self.btnClear.setObjectName("btnClear")
        self.btnAddChar = QtWidgets.QPushButton(self.centralwidget)
        self.btnAddChar.setGeometry(QtCore.QRect(70, 0, 60, 23))
        self.btnAddChar.setObjectName("btnAddChar")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(10, 0, 50, 23))
        self.lineEdit.setObjectName("lineEdit")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(280, 0, 58, 23))
        self.label.setObjectName("label")
        self.btnGo = QtWidgets.QPushButton(self.centralwidget)
        self.btnGo.setGeometry(QtCore.QRect(10, 30, 30, 23))
        self.btnGo.setObjectName("btnGo")
        self.btnTrain = QtWidgets.QPushButton(self.centralwidget)
        self.btnTrain.setGeometry(QtCore.QRect(200, 0, 71, 23))
        self.btnTrain.setObjectName("btnTrain")
        self.autoTrain = QtWidgets.QPushButton(self.centralwidget)
        self.autoTrain.setGeometry(QtCore.QRect(530, 0, 61, 23))
        self.autoTrain.setObjectName("autoTrain")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 597, 30))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnClear.setText(_translate("MainWindow", "clear"))
        self.btnAddChar.setText(_translate("MainWindow", "add"))
        self.label.setText(_translate("MainWindow", "Inform: "))
        self.btnGo.setText(_translate("MainWindow", "Go"))
        self.btnTrain.setText(_translate("MainWindow", "Обучать"))
        self.autoTrain.setText(_translate("MainWindow", "Авто"))
