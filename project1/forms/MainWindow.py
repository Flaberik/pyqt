# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(162, 85)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnLeft = QtWidgets.QPushButton(self.centralwidget)
        self.btnLeft.setGeometry(QtCore.QRect(0, 0, 40, 23))
        self.btnLeft.setObjectName("btnLeft")
        self.btnRight = QtWidgets.QPushButton(self.centralwidget)
        self.btnRight.setGeometry(QtCore.QRect(120, 0, 40, 23))
        self.btnRight.setObjectName("btnRight")
        self.number = QtWidgets.QLCDNumber(self.centralwidget)
        self.number.setGeometry(QtCore.QRect(50, 0, 64, 23))
        self.number.setObjectName("number")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 162, 30))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnLeft.setText(_translate("MainWindow", "<"))
        self.btnRight.setText(_translate("MainWindow", ">"))

